﻿/*
 * OaSharp 
 * Copyright 2020 Andreas Besting, info@besting-it.de
 * Licensed under the Apache License 2.0
 */

using Newtonsoft.Json;
using System;
using System.Threading;
using System.Threading.Tasks;
using HttpIot;

namespace OpenApiSharp
{
    public class OaSharp
    {
        private readonly HttpServer server = new HttpServer();

        public void StartServer(int port, CancellationToken token = default, byte maxConnections = 32)
        {
            server.Start(new HttpServerConfig()
            {
                Port = port,
                ConnectionBacklog = maxConnections
            }, token);
        }
        
        public void StartServer(HttpServerConfig config, CancellationToken token = default)
        {
            server.Start(config, token);
        }        
        
        public void StopServer()
        {
            server.Stop();
        }

        public void ClearRoutes() => server.ClearRoutes();
        
        public void AddRoute<T>(
            string path,
            Func<HttpRequest, HttpResponse, CancellationToken, Task<T>> result,
            HttpMethod method = HttpMethod.Post) where T : class
        {
            server.AddRoute(path, async (rq, rp, token) =>
            {
                var res = await result(rq, rp, token).ConfigureAwait(false);
                switch (res)
                {
                    case null:
                        return;
                    case string text:
                        await rp.AsText(text, token).ConfigureAwait(false);
                        return;
                    default:
                        rp.ContentType = "application/json";
                        await rp.AsText(JsonConvert.SerializeObject(res), token).ConfigureAwait(false);
                        break;
                }                
            } , method);
        }
        
        public void AddRoute(
            string path,
            Func<HttpRequest, HttpResponse, CancellationToken, Task> result,
            HttpMethod method = HttpMethod.Post)
        {
            server.AddRoute(path, async (rq, rp, token) =>
            {
                await result(rq, rp, token).ConfigureAwait(false);              
            } , method);
        }        

    }
}
