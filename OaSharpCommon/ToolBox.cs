﻿/*
 * OaSharp 
 * Copyright 2020 Andreas Besting, info@besting-it.de
 * Licensed under the Apache License 2.0
 */

using System.Collections.Generic;
using System.Text;

namespace OpenApiSharp
{
    public static class ToolBox
    {

        public static string StringFirstUpper(string s)
        {
            var sb = new StringBuilder(s);
            sb[0] = sb[0].ToString().ToUpper().ToCharArray()[0];
            return sb.ToString();
        }

        public static string ToReadableString(object inputObj)
        {
            if (inputObj == null)
                return null;
            var input = inputObj.ToString();
            var shortened = input.Replace("\r", "").Replace("\n", "").Replace("*/", "");
            int len;
            do
            {
                len = shortened.Length;
                shortened = shortened.Replace("  ", " ");
            } while (len != shortened.Length);
            return shortened;
        }

        public static HashSet<string> GetSchemaTypes(OpenApiSpecification openApiSpec)
        {
            var globalTypes = new HashSet<string>();

            foreach (var next in openApiSpec.Components.Schemas)
            {
                globalTypes.Add(next.Key);
            }

            return globalTypes;
        }

    }
}
