﻿/*
 * OaSharp 
 * Copyright 2020 Andreas Besting, info@besting-it.de
 * Licensed under the Apache License 2.0
 */

namespace OpenApiSharp
{
    internal enum HttpMethod
    {
        Get,
        Post,
        Put,
        Delete
    }
    
    internal class MethodInfo
    {
        public HttpMethod Method { get; set; }
        public string Path { get; set; }
        public string Name { get; set; }
        public string RequestType { get; set; }
        public string ResponseType { get; set; }
    }
}
