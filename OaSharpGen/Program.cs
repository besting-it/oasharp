﻿/*
 * OaSharp 
 * Copyright 2020 Andreas Besting, info@besting-it.de
 * Licensed under the Apache License 2.0
 */

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenApiSharp
{
    class Program
    {
        const string Version = "3.1.0";
        const string SpecParam = "spec";
        const string NamespaceParam = "namespace";
        const string OutParam = "out";

        static async Task Main(string[] args)
        {
            Console.WriteLine("OaSharp OpenAPI C# code generator v" + Version + ". Contact: info@besting-it.de\n");
            var specFile = GetArg(SpecParam, args);
            var nameSpace = GetArg(NamespaceParam, args);
            var outDir = GetArg(OutParam, args);
            bool paramError = false;
            if (specFile == null)
            {
                Console.WriteLine("Error: API specification filepath needed, use 'spec=[file]'");
                paramError = true;
            }
            if (nameSpace == null)
            {
                Console.WriteLine("Error: target namespace needed, use 'namespace=[namespace]'");
                paramError = true;
            }
            if (outDir == null)
            {
                Console.WriteLine("Hint: to specify output directory, use 'out=[path]'");
                outDir = string.Empty;
            }

            if (paramError)
                return;

            await Generate(specFile, nameSpace, outDir).ConfigureAwait(false);
        }

        private static async Task Generate(string specFile, string nameSpace, string outDir)
        {
            if (!string.IsNullOrEmpty(outDir))
            {
                Directory.CreateDirectory(outDir);
                if (!outDir.EndsWith(Path.DirectorySeparatorChar.ToString()))
                    outDir += Path.DirectorySeparatorChar;
            }
            var spec = await File.ReadAllTextAsync(specFile).ConfigureAwait(false);
            var openApiSpec = JsonConvert.DeserializeObject<OpenApiSpecification>(spec);
            await SchemaCodeGenerator.Generate(openApiSpec, nameSpace, outDir);
            var pathDict = openApiSpec.Paths.ToDictionary(p => p.Key, p => p.Value as JObject);
            var globalTypes = ToolBox.GetSchemaTypes(openApiSpec);
            string pathPrefix = string.Empty;
            var serverUrl = openApiSpec.Servers?.ToArray().FirstOrDefault()?.Url;
            if (serverUrl != null)
            {
                var url = new Uri(serverUrl);
                pathPrefix = url.AbsolutePath;
            }
            foreach (var pathInfo in pathDict)
            {
                var children = pathInfo.Value.Children();
                var httpPath = pathInfo.Key;
                var className = GenerateClassName(httpPath) + "Route";
                Console.WriteLine("Preparing class generation: " + className);
                var methodInfos = new List<MethodInfo>();
                foreach (JProperty path in children)
                {
                    if (!Enum.TryParse(ToolBox.StringFirstUpper(path.Name.ToLower()), out HttpMethod httpMethod))
                    {
                        Console.WriteLine("Error parsing Http method in path: " + path.Name);
                        return;
                    }
                    var opId = path.Value["operationId"];
                    if (opId == null)
                    {
                        Console.WriteLine("Missing 'operationId' in path, this is mandatory for code generation!");
                        return;
                    }
                    var operationName = ToolBox.StringFirstUpper(opId.ToString());
                    var requestBody = path.Value["requestBody"];
                    var requestHint = ToolBox.ToReadableString(requestBody);
                    if (requestHint == null)
                        requestHint = ToolBox.ToReadableString(path.Value["parameters"]);
                    var responses = path.Value["responses"];
                    var responseHint = ToolBox.ToReadableString(responses);

                    string requestType = null;
                    string responseType = null;

                    if (requestBody != null)
                        requestType = ResolveType(requestBody.ToString(), globalTypes);
                    if (responses != null)
                        responseType = ResolveType(responses.ToString(), globalTypes);

                    var finalPath = pathPrefix + httpPath;
                    Console.WriteLine("Path " + finalPath + " => Generating " + httpMethod + " route for operation: " + operationName);
                    methodInfos.Add(new MethodInfo()
                    {
                        Method = httpMethod,
                        Path = finalPath,
                        Name = operationName,
                        RequestType = requestType,
                        ResponseType = responseType
                    });
                }
                await ServerCodeGenerator.Generate(nameSpace, className, methodInfos, outDir);
            }
        }

        private static string ResolveType(string text, HashSet<string> globalTypes)
        {
            bool isArray = false;
            using (var sr = new StringReader(text))
            {
                string line = null;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Contains("$ref"))
                    {
                        var type = globalTypes.FirstOrDefault(t => line.Contains(t));
                        if (type != null)
                        {
                            return isArray? "List<" + type + ">" : type;
                        }
                    }
                    else if (line.Contains("type") && line.Contains("array"))
                        isArray = true;
                }
            }
            return null;
        }

        private static string GenerateClassName(string path)
        {
            var segments = path.Split(new[] { '/', '}', '{' }, StringSplitOptions.RemoveEmptyEntries);
            var pathSb = new StringBuilder();
            segments.ToList().ForEach(s =>
            {
                string segUpper = ToolBox.StringFirstUpper(s);
                pathSb.Append(segUpper);
            });
            return pathSb.ToString();
        }

        static string GetArg(string argOrPrefix, string[] args)
        {
            var p = args.FirstOrDefault(a => a.ToLowerInvariant().StartsWith(argOrPrefix));
            if (p == null)
                return null;
            int index = p.IndexOf('=');
            if (index == -1)
                return p;
            return p.Substring(index + 1);
        }

        static bool HasArg(string arg, string[] args) => GetArg(arg, args) != null;

    }

}
