﻿/*
 * OaSharp 
 * Copyright 2020 Andreas Besting, info@besting-it.de
 * Licensed under the Apache License 2.0
 */

using Newtonsoft.Json.Linq;
using NJsonSchema;
using NJsonSchema.CodeGeneration.CSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OpenApiSharp
{
    static class SchemaCodeGenerator
    {
        internal static async Task Generate(OpenApiSpecification openApiSpec, string nameSpace, string outDir)
        {
            var globalTypes = ToolBox.GetSchemaTypes(openApiSpec);
            globalTypes.ToList().ForEach(t => Console.WriteLine("Found type: " + t));

            // List of property names and resolved property types (which would be only of type object)
            var replaceableObjectTypes = new List<KeyValuePair<string, string>>();

            foreach (var next in openApiSpec.Components.Schemas)
            {
                var obj = next.Value as JObject;
                var props = obj["properties"];
                if (props != null)
                {
                    var children = props.Children();
                    foreach (JProperty child in children)
                    {
                        var str = child.Value.ToString();
                        if (str.Contains("\"$ref\":"))
                        {
                            string type = globalTypes.FirstOrDefault(k => str.Contains(k));
                            if (type == null)
                            {
                                Console.WriteLine("Warning, type can't be resolved for: " + str);
                            }
                            else
                            {
                                var propertyName = ToolBox.StringFirstUpper(child.Name);
                                replaceableObjectTypes.Add(new KeyValuePair<string, string>(propertyName, type));
                            }
                        }
                    }
                }

                var fileName = next.Key + ".cs";
                Console.WriteLine("Generating schema class: " + fileName);
                var schema = await JsonSchema.FromJsonAsync(PreProcess(next.Value.ToString()));
                var generator = new CSharpGenerator(schema);
                generator.Settings.Namespace = nameSpace;
                var content = generator.GenerateFile(next.Key);
                content = PostProcess(content, replaceableObjectTypes);
                await File.WriteAllTextAsync(outDir + fileName, content).ConfigureAwait(false);
            }
        }

        static string PreProcess(string spec)
        {
            spec = Regex.Replace(spec, "\"\\$ref.*\"", "\"type\": \"object\"");
            return spec;
        }

        static string PostProcess(string file, List<KeyValuePair<string, string>> replace)
        {
            var strReader = new StringReader(file);
            var sb = new StringBuilder();
            string next;
            while ((next = strReader.ReadLine()) != null)
            {
                foreach (var nextToken in replace)
                {
                    if (next.Contains(" " + nextToken.Key + " ") && next.TrimStart().StartsWith("public"))
                    {
                        next = next.Replace("object", nextToken.Value);
                    }
                }
                next = PostProcessEnums(next);
                sb.Append(next + Environment.NewLine);
            }
            return sb.ToString();
        }

        /// <summary>
        /// The official code generator assigns string enum integers starting at 1, instead of 0.
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        private static string PostProcessEnums(string next)
        {
            if (next.Contains("public enum"))
                enumIndicator++;
            if (next.Contains("}") && enumIndicator > 0)
                enumIndicator = 0;
            if (next.Contains("(Value = @") && enumIndicator == 1)
                enumIndicator++;
            else if (next.Contains(" = ") && enumIndicator == 2)
            {
                int start = next.IndexOf('=') + 1;
                if (int.TryParse(next.Substring(start, next.IndexOf(',') - start).Trim(), out int number))
                    next = next.Replace(number.ToString(), (number + 1).ToString());
                enumIndicator = 1;
            }
            return next;
        }

        private static int enumIndicator;
    }
}
