﻿/*
 * OaSharp 
 * Copyright 2020 Andreas Besting, info@besting-it.de
 * Licensed under the Apache License 2.0
 */

using Microsoft.CSharp;
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace OpenApiSharp
{

    /// <summary>
    /// CSharp source code generator for client code
    /// https://docs.microsoft.com/en-us/dotnet/framework/reflection-and-codedom/dynamic-source-code-generation-and-compilation
    /// </summary>
    class ServerCodeGenerator
    {
        private const string MethodNameSuffix = "Request";

        /// <summary>
        /// Generates client code.
        /// </summary>
        /// <param name="nameSpace">The target namespace</param>
        /// <param name="className">The class- and filename</param>
        /// <param name="methodInfos">List of method infos</param>/// 
        /// <returns></returns>
        internal static Task Generate(string nameSpace, string className, List<MethodInfo> methodInfos, string outDir)
        {
            var compileUnit = new CodeCompileUnit();
            var cns = new CodeNamespace(nameSpace);
            cns.Imports.Add(new CodeNamespaceImport("System"));
            cns.Imports.Add(new CodeNamespaceImport("System.Threading"));            
            cns.Imports.Add(new CodeNamespaceImport("System.Threading.Tasks"));
            cns.Imports.Add(new CodeNamespaceImport("System.Collections.Generic"));
            cns.Imports.Add(new CodeNamespaceImport("System.Net"));
            cns.Imports.Add(new CodeNamespaceImport("Newtonsoft.Json"));            
            cns.Imports.Add(new CodeNamespaceImport("OpenApiSharp"));
            cns.Imports.Add(new CodeNamespaceImport("HttpIot"));         
            compileUnit.Namespaces.Add(cns);
            var clazz = new CodeTypeDeclaration(className)
            {
                IsClass = true
            };
            cns.Types.Add(clazz);

            var routesInitMethod = new CodeMemberMethod
            {
                Name = "InitRoutes",
                Attributes = MemberAttributes.Public
            };
            routesInitMethod.Parameters.Add(new CodeParameterDeclarationExpression("OaSharp", "oaSharp"));
            clazz.Members.Add(routesInitMethod);

            foreach (var mi in methodInfos)
            {
                // Add request method content
                string methodName = mi.Name + MethodNameSuffix;
                AddRequestMethod(clazz, mi.RequestType, mi.ResponseType, methodName);
                // Init method call
                if (string.IsNullOrEmpty(mi.ResponseType))
                    AddSnippet(routesInitMethod, "oaSharp.AddRoute(\"" + mi.Path + "\", " + methodName + ", HttpMethod." + mi.Method + ")");
                else
                    AddSnippet(routesInitMethod, "oaSharp.AddRoute<" + mi.ResponseType + ">(\"" + mi.Path + "\", " + methodName + ", HttpMethod." + mi.Method + ")");
            }

            string fileName = className + ".cs";
            File.WriteAllText(outDir + fileName, GenerateCSharpCode(compileUnit));
            Console.WriteLine("Client code file written: " + fileName);
            return Task.CompletedTask;
        }
        
        private static void AddRequestMethod(CodeTypeDeclaration clazz, string requestType, string responseType, string methodName)
        {
            var routeFuncField = new CodeMemberField
            {
                Name = $"Func{methodName}",
                Attributes = MemberAttributes.Public
            };
            if (string.IsNullOrEmpty(requestType))
            {
                if (string.IsNullOrEmpty(responseType))
                    routeFuncField.Type = new CodeTypeReference($"Func<HttpRequest: HttpResponse: CancellationToken: Task>");
                else
                    routeFuncField.Type = new CodeTypeReference($"Func<HttpRequest: HttpResponse: CancellationToken: Task<{responseType}>>");
            }
            else
            {
                if (string.IsNullOrEmpty(responseType))
                    routeFuncField.Type = new CodeTypeReference($"Func<HttpRequest: {requestType}: HttpResponse: CancellationToken: Task>");
                else
                    routeFuncField.Type = new CodeTypeReference($"Func<HttpRequest: {requestType}: HttpResponse: CancellationToken: Task<{responseType}>>");
            }
            clazz.Members.Add(routeFuncField);
            var requestMethod = new CodeMemberMethod
            {
                Name = methodName,
                Attributes = MemberAttributes.Public
            };
            if (string.IsNullOrEmpty(responseType))
                requestMethod.ReturnType = new CodeTypeReference($"async Task");
            else
                requestMethod.ReturnType = new CodeTypeReference($"async Task<{responseType}>");
            var rq = new CodeParameterDeclarationExpression("HttpRequest", "rq");
            requestMethod.Parameters.Add(rq);
            var rp = new CodeParameterDeclarationExpression("HttpResponse", "rp");
            requestMethod.Parameters.Add(rp);
            var tk = new CodeParameterDeclarationExpression("CancellationToken", "token");
            requestMethod.Parameters.Add(tk);
            var returnPrefix = string.IsNullOrEmpty(responseType) ? string.Empty : "return ";
            if (string.IsNullOrEmpty(requestType))
            {
                AddSnippet(requestMethod, $"{returnPrefix}await Func{methodName}(rq, rp, token).ConfigureAwait(false)");
            }
            else
            {
                AddSnippet(requestMethod, $"var body = await rq.GetBody().ConfigureAwait(false)");
                AddSnippet(requestMethod, $"var input = JsonConvert.DeserializeObject<{requestType}>(body)");
                AddSnippet(requestMethod, $"{returnPrefix}await Func{methodName}(rq, input, rp, token).ConfigureAwait(false)"); 
            }
            clazz.Members.Add(requestMethod);
        }

        private static void AddSnippet(CodeMemberMethod requestMethod, string snippet)
        {
            var snippetExpression = new CodeExpressionStatement(new CodeSnippetExpression(snippet));
            requestMethod.Statements.Add(snippetExpression);
        }

        /// <summary>
        /// Generates the source code.
        /// </summary>
        /// <param name="compileunit">The compile unit</param>
        /// <returns>The source code</returns>
        public static string GenerateCSharpCode(CodeCompileUnit compileunit)
        {
            var provider = new CSharpCodeProvider();
            using var sw = new StringWriter();
            using var tw = new IndentedTextWriter(sw, "    ");
            var options = new CodeGeneratorOptions() { BracingStyle = "C" };
            provider.GenerateCodeFromCompileUnit(compileunit, tw, options);
            var code = sw.ToString();
            return code.Replace(":", ",");
        }
    }
}
