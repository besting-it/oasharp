# OaSharp

OaSharp is an OpenAPI / Swagger C# REST server code generator and hosting .NET standard library with only minimal dependencies.
No .NET Core / ASP.NET Core dependencies are needed.

Licensed under the Apache License 2.0

Further information on OpenAPI:

* https://www.openapis.org 
* https://swagger.io

Nuget hosting library to be used with generated code available at: https://www.nuget.org/packages/OaSharp/ (see further information below)

# Overview
OaSharp consists of two components:

* OaSharpGen: The code generator for schema classes and server routes.
* OsSharp: The HTTP REST library used by generated server routes.

You need an OpenAPI 3 compliant API specification in JSON format. For getting started, you can use the __petstore.json__ example (https://editor.swagger.io) as shown in the next section.

## Example using code generation tool

* You can download the binary command line code generator tool [__OaSharpGen 3__](https://bitbucket.org/besting-it/oasharp/downloads/OaSharpGen-3.1.0.zip) for generating server code. The petstore example is included.
* For older versions below 3.1.0 use the [legacy generator2](https://bitbucket.org/besting-it/oasharp/downloads/OaSharpGen-Legacy-2-3.zip)
* For older 1.x.x versions use the [legacy generator1](https://bitbucket.org/besting-it/oasharp/downloads/OaSharpGen-Legacy-1.zip)

From the command line, launch OaShapGen like this:
```
OaSharpGen spec=petstore.json namespace=MyNamespace out=Code
```
OaSharpGen produces the following output:

```
OaSharp OpenAPI C# code generator v2.x.x Contact: info@besting-it.de

Found type: Order
Found type: Category
Found type: User
Found type: Tag
Found type: Pet
Found type: ApiResponse
Generating schema class: Order.cs
Generating schema class: Category.cs
Generating schema class: User.cs
Generating schema class: Tag.cs
Generating schema class: Pet.cs
Generating schema class: ApiResponse.cs
Preparing class generation: PetRoute
Path /v2/pet => Generating Put route for operation: UpdatePet
Path /v2/pet => Generating Post route for operation: AddPet
Client code file written: PetRoute.cs
Preparing class generation: PetFindByStatusRoute
Path /v2/pet/findByStatus => Generating Get route for operation: FindPetsByStatus
Client code file written: PetFindByStatusRoute.cs
...
[snipped]
```

The code generator starts by generating schema data classes like __Order.cs__, __Category.cs__ or __Pet.cs__. After that, classes for different routing paths and HTTP methods are generated.
Some classes like __PetRoute.cs__ are bundling several methods for different HTTP methods (e.g. PUT and POST) using the same path.

Each route class contains an __InitRoutes()__ method to be called each for initializing the routes. In case of __PetRoute.cs__ this is:

```csharp
public void InitRoutes(OaSharp oaSharp)
{
    oaSharp.AddRoute("/v2/pet", UpdatePetRequest, HttpMethod.Put);
    oaSharp.AddRoute("/v2/pet", AddPetRequest, HttpMethod.Post);
}
```

Furthermore, request callback methods stubs are generated, in case of __PetRoute.cs__ these are:

```csharp
public class PetRoute
{	
    public Func<HttpRequest, Pet, HttpResponse, CancellationToken, Task> FuncUpdatePetRequest;
    
    public Func<HttpRequest, Pet, HttpResponse, CancellationToken, Task> FuncAddPetRequest;
    
    public void InitRoutes(OaSharp oaSharp)
    {
        oaSharp.AddRoute("/v2/pet", UpdatePetRequest, HttpMethod.Put);
        oaSharp.AddRoute("/v2/pet", AddPetRequest, HttpMethod.Post);
    }
    
    public async Task UpdatePetRequest(HttpRequest rq, HttpResponse rp, CancellationToken token)
    {
        var body = await rq.GetBody().ConfigureAwait(false);
        var input = JsonConvert.DeserializeObject<Pet>(body);
        await FuncUpdatePetRequest(rq, input, rp, token).ConfigureAwait(false);
    }
    
    public async Task AddPetRequest(HttpRequest rq, HttpResponse rp, CancellationToken token)
    {
        var body = await rq.GetBody().ConfigureAwait(false);
        var input = JsonConvert.DeserializeObject<Pet>(body);
        await FuncAddPetRequest(rq, input, rp, token).ConfigureAwait(false);
    }
}
```

In this case, the PUT and POST methods expect an HTTP client to transfer _Pet_ objects in JSON format inside the HTTP body which are automatically parsed and converted into objects and are available in the __input__ variable.
The HTTP responses contain no data, only a HTTP status code (in case of success, HTTP 200). Code for storing and updating the data as well as producing suitable error cases described by the comments must be implemented.

In order to process the requests you have to provide the actual implementation of __FuncUpdatePetRequest__ and __FuncAddPetRequest__:

```csharp
// Create server instance
var oaSharp = new OaSharp();

// Init routes
var route = new PetRoute();
route.FuncUpdatePetRequest = async (rq, input, rp, token) => 
{
    // Your code here
};

route.FuncAddPetRequest = async (rq, input, rp, token) => 
{
    // Your code here
};

// Initialize
route.InitRoutes(oaSharp);
```

In case of other routes, the HTTP request may contain only parameters provided by HTTP GET in the URL or in case of HTTP POST by variables stored in the HTTP body.
These parameters are available in a dictionary. For example, the __PetFindByStatusRoute.cs__ contains  a generated method with parameter request and object response:

```csharp
public class PetFindByStatusRoute
{
    public Func<HttpRequest, HttpResponse, CancellationToken, Task<List<Pet>>> FuncFindPetsByStatusRequest;
    
    public void InitRoutes(OaSharp oaSharp)
    {
        oaSharp.AddRoute<List<Pet>>("/v2/pet/findByStatus", FindPetsByStatusRequest, HttpMethod.Get);
    }
    
    public async Task<List<Pet>> FindPetsByStatusRequest(HttpRequest rq, HttpResponse rp, CancellationToken token)
    {
        return await FuncFindPetsByStatusRequest(rq, rp, token).ConfigureAwait(false);
    }
}
```

For processing __arguments__ (path-, url- or form-parameters) use the HttpRequest parameter (see further information below).

## Based on HttpIot

OaSharp is based on [HttpIot](https://bitbucket.org/besting-it/httpiot), in case you need to use the HttpRequest or HttpResponse directly. Please refer to the documentation.

## Server start

To actually use the generated code in your own projects, create a new C# project, add all generated source files and install the OaSharp [nuget](https://www.nuget.org/packages/OaSharp) packgage.

Then finish implementing the method stubs.
Remember to call all __InitRoutes()__ methods before starting the server. Finally, start the OaSharp HTTP REST server by calling the __StartServer()__ method. For example:

```csharp
static void Main(string[] args)
{
    // Create server instance
    var oaSharp = new OaSharp();
    
    // Init routes
    // ...
    
    oaSharp.StartServer(8090);
    Console.Read();
}
```

You may change the port and optionally activate HTTPs, add a cancellation token and a different value for changing the number of concurrent connections.

## Official code generator

The [official code generator](https://github.com/OpenAPITools/openapi-generator) supports multiple (other) languages.

### Example for generating Java client code
Here's an example on how to generate Java 11 client code:
```
java -jar openapi-generator-cli-5.0.0.jar generate -g java -i petstore.json -o .\output --additional-properties library=native
```